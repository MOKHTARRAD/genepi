﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Newtonsoft.Json;
using MongoDB.Driver;
using Genepi.Models;
using Genepi.Settings;

namespace Genepi.Services
{
    public class TestService
    {
        private readonly IMongoCollection<Test> _tests;

        public TestService(IConfig config)
        {
            string ConnectionString;
            string DatabaseName;
            string TestsCollectionName;

            try
            {
                ConnectionString = config.Mongodb.Url;
                DatabaseName = config.Mongodb.Db;
                TestsCollectionName = config.Mongodb.Collections.Tests;
            } catch(Exception e)
            {
                Console.WriteLine(e.ToString());
                throw;
            }

            var client = new MongoClient(ConnectionString);
            var database = client.GetDatabase(DatabaseName);

            _tests = database.GetCollection<Test>(TestsCollectionName);


        }

        public List<Test> Get() =>
            _tests.Find(test => true).ToList();

        public Test Get(string id) =>
            _tests.Find<Test>(test => test.Id == id).FirstOrDefault();

        public Test Create(Test test)
        {
            _tests.InsertOne(test);
            return test;
        }

        public void Update(string id, Test testIn) =>
            _tests.ReplaceOne(test => test.Id == id, testIn);

        public void Remove(Test testIn) =>
            _tests.DeleteOne(test => test.Id == testIn.Id);

        public void Remove(string id) =>
            _tests.DeleteOne(test => test.Id == id);
    }
}
