﻿using EasyNetQ;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Genepi.Services.RabbitMQ
{

    public class UserRequest
    {
        public long Id { get; set; }
        
        public UserRequest(long id)
        {
            Id = id;
        }
    }

    public class UserResponse
    {
        public string Name { get; set; }

        public UserResponse() { }
    }

    public class UserEventHandler : BackgroundService
    {
        private readonly IBus _bus;

        public UserEventHandler(IBus bus)
        {
            _bus = bus;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            await _bus.Rpc.RespondAsync<UserRequest, UserResponse>(ProcessUserRequest);
        }

        //Producer
        private UserResponse ProcessUserRequest(UserRequest userRequest)
        {
            return new UserResponse() { Name = "Feralz" };
        }
    }
}
