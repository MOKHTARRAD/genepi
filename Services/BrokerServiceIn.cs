﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Genepi.Settings;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using EasyNetQ;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using Genepi.Models;
using Genepi.Controllers.API;
using Microsoft.AspNetCore.Mvc;

using Microsoft.AspNetCore.SignalR;
using Genepi.Hubs;
using Microsoft.Extensions.Hosting;
using System.Threading;

namespace Genepi
{
    public class BrokerServiceIn : BackgroundService
    {
        private IServiceProvider _sp;
        private ConnectionFactory _factory;
        private IConnection _connection;
        private IModel _channel;

        private int Count = 0;

        private Uri RabbitmqUri = new("amqp://guest:guest@localhost:5672/");
        //private Uri RabbitmqUri = new("amqp://guest:guest@192.168.100.195:5672");
        private string RabbitmqConnectionString = "host=localhost;username=guest;password=guest;timeout=60";
        private string RabbitmqQueueNameIn = "p42In";
        public bool Connected { get; set; }

        private IHubContext<BrokerHub> HubContext;

        public BrokerServiceIn() { }
        public BrokerServiceIn(IConfig config, IServiceProvider sp, IHubContext<BrokerHub> hubcontext)
        {
            RabbitmqUri = new Uri(config.RabbitMQ.UrlLocal);
            RabbitmqConnectionString = config.RabbitMQ.ConnectionString;
            RabbitmqQueueNameIn = config.RabbitMQ.QueueNameIn;

            _sp = sp;
            _factory = new ConnectionFactory() { HostName = "localhost" };
            _connection = _factory.CreateConnection();
            _channel = _connection.CreateModel();
            _channel.QueueDeclare(
                queue: RabbitmqQueueNameIn,
                durable: true,
                exclusive: false,
                autoDelete: false,
                arguments: null);

            HubContext = hubcontext;
        }

        private static BrokerServiceIn _Instance;
        public static BrokerServiceIn GetInstance()
        {
            return _Instance ??= new BrokerServiceIn();
        }

        // send raw request to RabbitMQ
        public async void Produce(string id, string data)
        {
            if (Connected)
            {
                Console.WriteLine("\nProduceIn()");
                var factory = new ConnectionFactory { Uri = RabbitmqUri };
                using var connection = factory.CreateConnection();
                using var channel = connection.CreateModel();
                channel.QueueDeclare(
                    RabbitmqQueueNameIn,
                    durable: true,
                    exclusive: false,
                    autoDelete: false,
                    arguments: null);

                var msg = new { Name = "ProducerIn", Message = await TestingProcessIn(data) };
                var body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(msg));

                channel.BasicPublish("", RabbitmqQueueNameIn, null, body);

                //_ = HubContext.Clients.Client(id).SendAsync("Response", msg);
            }
        }

        // BrokerService ConsumeIn
        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            // when the service is stopping
            // dispose these references
            // to prevent leaks
            if (stoppingToken.IsCancellationRequested)
            {
                _channel.Dispose();
                _connection.Dispose();
                return Task.CompletedTask;
            }

            // create a consumer that listens on the channel (queue)
            var consumer = new EventingBasicConsumer(_channel);

            // handle the Received event on the consumer
            // this is triggered whenever a new message
            // is added to the queue by the producer
            consumer.Received += async (model, ea) =>
            {
                // read the message bytes
                var body = ea.Body.ToArray();
                // convert back to the original string
                var msg = Encoding.UTF8.GetString(body);

                Console.WriteLine($"[x] Received In \"msg\" = {msg}");
                MqttObject mqMessage = JsonConvert.DeserializeObject<MqttObject>(msg);
                BrokerRequest request = JsonConvert.DeserializeObject<BrokerRequest>(mqMessage.Message.Value);

                Console.WriteLine(" [x] Received In {0}", request.Message);

                // transformation réponse
                Coords coords = JsonConvert.DeserializeObject<Coords>(request.Message);
                ActionResult<string> response = await WebServicesController.GetInstance().getRouteByType(coords);
                BrokerServiceOut.GetInstance().Produce(request.Id, response.Value);


                _ = Task.Run(() =>
                  {
                    // Something to do...
                });
            };

            //_channel.BasicConsume(queue: "heroes", autoAck: true, consumer: consumer);
            _channel.BasicConsume(RabbitmqQueueNameIn, true, consumer);

            return Task.CompletedTask;
        }

        public void TryToConnect(IServiceCollection services)
        {
            if (CheckRabbitMQStatus())
            {
                try
                {
                    var bus = RabbitHutch.CreateBus(RabbitmqConnectionString);
                    services.AddSingleton(bus);
                    services.AddHostedService<Services.RabbitMQ.UserEventHandler>();
                    Connected = true;
                    Console.WriteLine("TryToConnect() succeeded...");
                }
                catch (RabbitMQ.Client.Exceptions.BrokerUnreachableException e)
                {
                    Console.WriteLine("TryToConnect() failed...");
                    Console.WriteLine(e);
                }
            }
        }

        private bool CheckRabbitMQStatus()
        {
            ConnectionFactory factory = new() { Uri = RabbitmqUri };
            try
            {
                IConnection conn = factory.CreateConnection();
                conn.Close();
                conn.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                if (ex.Message == "None of the specified endpoints were reachable")
                {
                    //send mail MQ is down
                }
            }
            return false;
        }

        public async Task<ActionResult<string>> TestingProcessIn(string msg)
        {
            await Task.Delay(0);
            return msg;
        }
    }
}
