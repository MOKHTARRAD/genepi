﻿using Genepi.Models;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;

namespace Genepi.Hubs
{
    public class BrokerHub : Hub
    {
        private static BrokerHub _Instance;
        public static BrokerHub GetInstance()
        {
            return _Instance ??= new BrokerHub();
        }

        public Task Request(string msg)
        {
            BrokerRequest request = new BrokerRequest() { Id = Context.ConnectionId, Message = msg };
            BrokerServiceIn.GetInstance().Produce(request.Id, JsonConvert.SerializeObject(request));

            //return Clients.Caller.SendAsync("Request", "Processing your request...");
            return Clients.Client(Context.ConnectionId).SendAsync("Response", msg);
        }

        /*
         * EXAMPLE FUNCTIONS
         * 
        public Task SendMessageToAll(string msg)
        {
            return Clients.All.SendAsync("ReceiveMessage", msg);
        }

        public Task SendMessageToCaller(string msg)
        {
            return Clients.Caller.SendAsync("ReceiveMessage", msg);
        }

        public Task SendMessageToUser(string connectionId, string msg)
        {
            return Clients.Client(connectionId).SendAsync("ReceiveMessage", msg);
        }

        public Task JoinGroup(string group)
        {
            return Groups.AddToGroupAsync(Context.ConnectionId, group);
        }

        public Task SendMessageToGroup(string group, string msg)
        {
            return Clients.Group(group).SendAsync("ReceiveMessage", msg);
        }

        public override async Task OnConnectedAsync()
        {
            await base.OnConnectedAsync();
        }

        public override Task OnDisconnectedAsync(Exception ex)
        {
            return base.OnDisconnectedAsync(ex);
        }
        */
    }
}
