﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Genepi.Controllers.API
{
    [Route("api")]
    [ApiController]
    public class UserDataController : Controller
    {
        static readonly HttpClient client = new();

        // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
        public class Step
        {
            public double distance { get; set; }
            public double duration { get; set; }
            public int type { get; set; }
            public string instruction { get; set; }
            public string name { get; set; }
            public List<int> way_points { get; set; }
        }

        public class Segment
        {
            public double distance { get; set; }
            public double duration { get; set; }
            public List<Step> steps { get; set; }
        }

        public class Summary
        {
            public double distance { get; set; }
            public double duration { get; set; }
        }

        public class Properties
        {
            public List<Segment> segments { get; set; }
            public Summary summary { get; set; }
            public List<int> way_points { get; set; }
        }

        public class Geometry
        {
            public List<List<double>> coordinates { get; set; }
            public string type { get; set; }
        }

        public class Feature
        {
            public List<double> bbox { get; set; }
            public string type { get; set; }
            public Properties properties { get; set; }
            public Geometry geometry { get; set; }
        }

        public class Query
        {
            public List<List<double>> coordinates { get; set; }
            public string profile { get; set; }
            public string format { get; set; }
        }

        public class Engine
        {
            public string version { get; set; }
            public DateTime build_date { get; set; }
            public DateTime graph_date { get; set; }
        }

        public class Metadata
        {
            public string attribution { get; set; }
            public string service { get; set; }
            public long timestamp { get; set; }
            public Query query { get; set; }
            public Engine engine { get; set; }
        }

        public class Root
        {
            public string type { get; set; }
            public List<Feature> features { get; set; }
            public List<double> bbox { get; set; }
            public Metadata metadata { get; set; }
        }


        //[HttpPost("userData")]
        ////TODO remplacer string par le bon truc, on recupere depuis rabbitMQ
        //public async Task<ActionResult<string>> GetUserData(String Lat, String Long)
        //{
        //    //Send data to RabbitMQ
        //    Console.WriteLine("Lat" + Lat);
        //    Console.WriteLine("Long" + Long);
        //    //Todo
        //    return null;
        //}

        ////TODO remplacer string par le bon truc, on recupere depuis rabbitMQ
        //public static async Task<ActionResult<string>> SetCarRouteAsync(string geoJsonData)
        //{
        //    // Pour un trajet en voiture
        //    // Envoyer l'info à RabbitMQ que j'ai recu les infos et que je les traite
        //    // TODO: le return doit renvoyer a rabbitMQ

        //    return geoJsonData;
        //}
    }
}
