﻿using Genepi.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Genepi.Controllers.API
{
    public class WebServicesController
    {
        static readonly HttpClient client = new();
        readonly string api_key = "5b3ce3597851110001cf62489a727653f4854a4f9c2f0bff539eae3c";
        readonly string base_url = "https://api.openrouteservice.org/v2/";

        private static WebServicesController _Instance;
        public static WebServicesController GetInstance()
        {
            return _Instance ??= new WebServicesController();
        }


        //TODO remplacer string par le bon truc, on recupere depuis rabbitMQ
        public async Task<ActionResult<string>> getRouteByType([FromBody] Coords coords)
        {
            // Appel RabbitMQ pour avoir le retour des infos d'Arnaud.
            string RabbitMQReturn = coords.StartLat + "#" + coords.StartLong + "#" + coords.StopLat + "#" + coords.StopLong + "#" + coords.TransportType;
            string[] splitedInfos = RabbitMQReturn.Split('#');

            Coords userStartAndEndPoint = new Coords();
            userStartAndEndPoint.StartLat = float.Parse(splitedInfos[0]);
            userStartAndEndPoint.StartLong = float.Parse(splitedInfos[1]);
            userStartAndEndPoint.StopLat = float.Parse(splitedInfos[2]);
            userStartAndEndPoint.StopLong = float.Parse(splitedInfos[3]);

            string TransportType = splitedInfos[4];

            switch (TransportType)
            {
                case "driving-car":
                    return await GetCarRouteAsync(userStartAndEndPoint);
                case "cycling-regular":
                    return await GetCyclingRouteAsync(userStartAndEndPoint);
                case "foot-walking":
                    return await GetFootRouteAsync(userStartAndEndPoint);
                case "velov":
                    return await GetVelovStationsAsync();
                case "all":
                default:
                    return await GetFastestRoute(userStartAndEndPoint);
            }
        }

        public async Task<ActionResult<string>> GetFastestRoute([FromBody] Coords coords)
        {
            try
            {
                ActionResult<string> carRoute = await GetCarRouteAsync(coords);
                ActionResult<string> cyclingRoute = await GetCyclingRouteAsync(coords);
                ActionResult<string> footRoute = await GetFootRouteAsync(coords);

                JObject carRouteDeserialized = JsonConvert.DeserializeObject<JObject>(carRoute.Value);
                JObject cyclingRouteDeserialized = JsonConvert.DeserializeObject<JObject>(cyclingRoute.Value);
                JObject footRouteDeserialized = JsonConvert.DeserializeObject<JObject>(footRoute.Value);

                int carRouteDuration = (int)carRouteDeserialized["features"][0]["properties"]["summary"]["duration"];
                int cyclingRouteDuration = (int)cyclingRouteDeserialized["features"][0]["properties"]["summary"]["duration"];
                int footRouteDuration = (int)footRouteDeserialized["features"][0]["properties"]["summary"]["duration"];

                int faster = Math.Min(carRouteDuration, Math.Min(cyclingRouteDuration, footRouteDuration));

                if (faster == carRouteDuration)
                {
                    return await GetCarRouteAsync(coords);
                }
                else if (faster == cyclingRouteDuration)
                {
                    return await GetCyclingRouteAsync(coords);
                }
                else if (faster == footRouteDuration)
                {
                    return await GetFootRouteAsync(coords);
                }

                return null;
            } catch (Exception e) {
                return "{0} Exception caught." + e;
            }
        }
        
        /*[HttpPost("extradata")]
        public async Task<ActionResult<string>> GetExtraData(string data)
        {
            Console.WriteLine(data);
            var baseAddress = new Uri("https://api.openrouteservice.org/v2/");
            HttpClient httpClient = new HttpClient { BaseAddress = baseAddress };
            httpClient.DefaultRequestHeaders.Clear();
            httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Accept", "text/plain");
            httpClient.DefaultRequestHeaders.TryAddWithoutValidation("contentType", "application/json; charset=utf-8");
            httpClient.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", "5b3ce3597851110001cf62489a727653f4854a4f9c2f0bff539eae3c");

            // quotes might have to be escaped
            var content = new StringContent(data);
            var response1 = await httpClient.PostAsync("matrix/driving-car", content);
            var response2 = await httpClient.PostAsync("matrix/cycling-regular", content);
            var response3 = await httpClient.PostAsync("matrix/foot-walking", content);
            Console.WriteLine("response1", response1);
            string responseData1 = await response1.Content.ReadAsStringAsync();
            string responseData2 = await response2.Content.ReadAsStringAsync();
            string responseData3 = await response3.Content.ReadAsStringAsync();
            Console.WriteLine("responseData1", responseData1);

            return responseData1 + '#' + responseData2 + "#" + responseData3;
        }*/

        [HttpPost("carroute")]
        //TODO remplacer string par le bon truc, on recupere depuis rabbitMQ
        public async Task<ActionResult<string>> GetCarRouteAsync([FromBody] Coords coords)
        {
            string url = base_url + "directions/driving-car?api_key=" + api_key + "&start=" + coords.StartLong.ToString(CultureInfo.InvariantCulture) + "," + coords.StartLat.ToString(CultureInfo.InvariantCulture) + "&end=" + coords.StopLong.ToString(CultureInfo.InvariantCulture) + "," + coords.StopLat.ToString(CultureInfo.InvariantCulture);
            HttpResponseMessage response = await client.GetAsync(url);
            response.EnsureSuccessStatusCode();
            string responseBody = await response.Content.ReadAsStringAsync();
            //TODO: le return doit renvoyer a rabbitMQ
            return responseBody;
        }

        [HttpPost("cyclingroute")]
        //TODO remplacer string par le bon truc, on recupere depuis rabbitMQ'
        public async Task<ActionResult<string>> GetCyclingRouteAsync([FromBody] Coords coords)
        {
            string url = base_url + "directions/cycling-regular?api_key=" + api_key + "&start=" + coords.StartLong.ToString(CultureInfo.InvariantCulture) + "," + coords.StartLat.ToString(CultureInfo.InvariantCulture) + "&end=" + coords.StopLong.ToString(CultureInfo.InvariantCulture) + "," + coords.StopLat.ToString(CultureInfo.InvariantCulture);
            HttpResponseMessage response = await client.GetAsync(url);
            response.EnsureSuccessStatusCode();
            string responseBody = await response.Content.ReadAsStringAsync();
            //TODO: le return doit renvoyer a rabbitMQ
            return responseBody;
        }

        [HttpPost("footroute")]
        public async Task<ActionResult<string>> GetFootRouteAsync([FromBody] Coords coords)
        {
            string url = base_url + "directions/foot-walking?api_key=" + api_key + "&start=" + coords.StartLong.ToString(CultureInfo.InvariantCulture) + "," + coords.StartLat.ToString(CultureInfo.InvariantCulture) + "&end=" + coords.StopLong.ToString(CultureInfo.InvariantCulture) + "," + coords.StopLat.ToString(CultureInfo.InvariantCulture);
            HttpResponseMessage response = await client.GetAsync(url);
            response.EnsureSuccessStatusCode();
            string responseBody = await response.Content.ReadAsStringAsync();
            //TODO: le return doit renvoyer a rabbitMQ
            return responseBody;
        }


        [HttpPost("velov")]
        public async Task<ActionResult<string>> GetVelovStationsAsync()
        {
            //Pour un trajet à pied

            string base_url = "https://public.opendatasoft.com/api/records/1.0/search/";
            String url = base_url + "?dataset=station-velov-grand-lyon&q=&rows=400";
            HttpResponseMessage response = await client.GetAsync(url);
            response.EnsureSuccessStatusCode();
            string responseBody = await response.Content.ReadAsStringAsync();
            //TODO: le return doit renvoyer a rabbitMQ
            return responseBody;
        }
    }
}
