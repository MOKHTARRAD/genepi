using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;

using Microsoft.Extensions.Options;
using Genepi.Services;
using Genepi.Settings;

using EasyNetQ;
using RabbitMQ.Client;
using Genepi.Hubs;
using Microsoft.AspNetCore.SignalR;
using RabbitMQ.Client.Events;
using System.Text;

namespace Genepi
{
    public class Startup
    {
        private int Count = 0;

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // requires using Microsoft.Extensions.Options
            services.Configure<Config>(Configuration.GetSection(nameof(Config)));

            services.AddSingleton<IConfig>(sp => sp.GetRequiredService<IOptions<Config>>().Value);
            services.AddSingleton<TestService>();

            services.AddControllersWithViews();

            // connecting to RabbitMQ
            BrokerServiceIn mqIn = BrokerServiceIn.GetInstance();
            mqIn.TryToConnect(services);
            if (!mqIn.Connected) Console.WriteLine("RabbitMQ Server not reachable.");
            else Console.WriteLine("RabbitMQ successfully loaded.");
            StartRabbitMQServer(services);

            services.AddHostedService<BrokerServiceIn>();

            BrokerServiceOut mqOut = BrokerServiceOut.GetInstance();
            mqOut.TryToConnect(services);
            if (!mqOut.Connected) Console.WriteLine("RabbitMQ Server not reachable.");
            else Console.WriteLine("RabbitMQ successfully loaded.");
            StartRabbitMQServer(services);

            services.AddHostedService<BrokerServiceOut>();

            // connecting to realtime
            services.AddSignalR();
            services.AddScoped<BrokerServiceOut>();
            services.AddControllers().AddJsonOptions(options =>
            {
                options.JsonSerializerOptions.PropertyNamingPolicy = null;
            });

        }

        private static void StartRabbitMQServer(IServiceCollection services)
        {
            if (CheckRabbitMQStatus())
            {
                try
                {
                    string rabbitmqConnectionString = "host=localhost;username=guest;password=guest;timeout=60";
                    var bus = RabbitHutch.CreateBus(rabbitmqConnectionString);
                    services.AddSingleton(bus);
                    services.AddHostedService<Services.RabbitMQ.UserEventHandler>();
                }
                catch (RabbitMQ.Client.Exceptions.BrokerUnreachableException e)
                {
                    Console.WriteLine(e);
                }
            }
        }

        private static bool CheckRabbitMQStatus()
        {
            ConnectionFactory factory = new ConnectionFactory
            {
                Uri = new Uri("amqp://guest:guest@localhost:5672/")
            };
            IConnection conn = null;
            try
            {
                conn = factory.CreateConnection();
                conn.Close();
                conn.Dispose();
                return true;
            }
            catch (Exception ex)
            {
                if (ex.Message == "None of the specified endpoints were reachable")
                {
                    //send mail MQ is down
                }
            }
            return false;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(name: "default", pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapHub<BrokerHub>("/broker");
            });

            app.UseStaticFiles();
        }
    }
}
