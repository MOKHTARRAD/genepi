﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Genepi.Settings.MongoDBNS;

namespace Genepi.Settings
{
    public class RabbitMQ : IRabbitMQ
    {
        public string Url { get; set; }
        public string UrlLocal { get; set; }
        public string ConnectionString { get; set; }
        public string QueueNameIn { get; set; }
        public string QueueNameOut { get; set; }
    }

    public interface IRabbitMQ
    {
        string Url { get; set; }
        string UrlLocal { get; set; }
        string ConnectionString { get; set; }
        string QueueNameIn { get; set; }
        string QueueNameOut { get; set; }

    }
}
