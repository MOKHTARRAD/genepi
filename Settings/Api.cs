﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Genepi.Settings.ApiNS
{
    public class Api : IApi
    {
        public OpenRouteService OpenRouteService { get; set; }
    }

    public interface IApi
    {
        OpenRouteService OpenRouteService { get; set; }
    }
}
