﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Genepi.Settings.MongoDBNS;

namespace Genepi.Settings
{
    public class MongoDB : IMongoDB
    {
        public string Url { get; set; }
        public string Db { get; set; }
        public Collections Collections { get; set; }
    }

    public interface IMongoDB
    {
        string Url { get; set; }
        string Db { get; set; }
        Collections Collections { get; set; }
    }
}
