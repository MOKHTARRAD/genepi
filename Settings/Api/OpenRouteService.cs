﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Genepi.Settings.ApiNS
{
    public class OpenRouteService : IOpenRouteService
    {
        public string Url { get; set; }
        public string Key { get; set; }
    }

    public interface IOpenRouteService
    {
        string Url { get; set; }
        string Key { get; set; }
    }
}
