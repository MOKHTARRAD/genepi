﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Genepi.Models
{
    public class Coords
    {
        public float StartLat { get; set; }
        public float StartLong { get; set; }
        public float StopLat { get; set; }
        public float StopLong { get; set; }
        public string TransportType { get; set; }
    }
}
