﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Genepi.Models
{
    public class MqttObject
    {
        public string Name { get; set; }
        public MqttMessage Message { get; set; }
    }
}
