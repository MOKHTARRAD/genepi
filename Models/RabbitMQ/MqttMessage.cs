﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Genepi.Models
{
    public class MqttMessage
    {
        public string Result { get; set; }
        public string Value { get; set; }
    }
}
