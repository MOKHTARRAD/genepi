﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Genepi.Models
{
    public class Coordinate
    {
        public decimal Latitude { get; set; }

        public decimal Longitude { get; set; }
    }
}
